import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatListModule,
  MatMenuModule, MatPaginatorModule,
  MatStepperModule,
  MatTabsModule,
  MatInputModule
} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AuthService} from './services/auth/auth.service';
import {SessionTimerService} from './services/session/session-timer.service';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {LoginComponent} from './components/login/login.component';
import {SettingsComponent} from './components/settings/settings.component';
import {NumberInputComponent} from './components/number-input/number-input.component';
import { SessionComponent } from './components/session/session.component';
import { BreakComponent } from './components/break/break.component';
import { TasksComponent } from './components/tasks/tasks.component';
import {ClockComponent} from './components/clock/clock.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {FormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {BreakTimerService} from './services/break/break-timer.service';

export const MD_MODULES = [ MatListModule, MatIconModule, MatButtonModule, MatMenuModule, MatInputModule,
                            MatTabsModule, MatDialogModule, MatStepperModule, MatExpansionModule, MatTableModule, MatPaginatorModule];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    SettingsComponent,
    NumberInputComponent,
    SessionComponent,
    BreakComponent,
    TasksComponent,
    ClockComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase, 'pomodoro-app'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    ...MD_MODULES
  ],
  providers: [AuthService, SessionTimerService, BreakTimerService],
  bootstrap: [AppComponent],
  entryComponents: [WelcomeComponent, LoginComponent]

})
export class AppModule { }
