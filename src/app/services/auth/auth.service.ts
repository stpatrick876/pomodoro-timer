import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;
  private logInOutEvent: Subject<boolean> = new Subject<boolean>();
  public logInOutEventAnnounced$ = this.logInOutEvent.asObservable();


  constructor(public afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
  }

  googleLogin() {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  anonLogin() {
    return this.afAuth.auth.signInAnonymously();
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  /**
   * trigger event to notify sign in and sign out;
   * @param state - true = logIn, false = logOut
   *
   */
  notifyLogInOutEvent(state: boolean) {
    this.logInOutEvent.next(state);
  }

}
