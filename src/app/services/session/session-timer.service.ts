import {Injectable} from '@angular/core';
import 'rxjs';
import { Howl } from 'howler';
import {isBoolean, isUndefined} from 'util';
import {BehaviorSubject, interval, Subject} from 'rxjs';
import {map, take, takeUntil} from 'rxjs/operators';
import {Timer} from '../../models/Timer';
import {TimerState, TimerType} from '../../models/common.enum';
import {Task} from 'src/app/models/Task.model';

@Injectable()
export class SessionTimerService {
  public timer: Timer;
  private start = 59;
  public timer$ = new BehaviorSubject(new Timer());
  private progressEvent: Subject<any> = new Subject<any>();
  public progressEventAnnounced$ = this.progressEvent.asObservable();
  private startTimerEvent: Subject<TimerType> = new Subject<TimerType>();
  public startTimerEventAnnounced$ = this.startTimerEvent.asObservable();
  private countdownEndEvent: Subject<Task> = new Subject<Task>();
  public sessionEndEventAnnounced$ = this.countdownEndEvent.asObservable();
  private pause$: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  init(task: Task, startTime: number, type: TimerType) {
    this.timer = new Timer().initializeTimer(startTime, task, type);
    return this.timer;
  }

  countDown(type?: TimerType) {
    const $this = this;
    if (type) {
      this.timer.type = type;
    }

      interval(1000)
        .pipe(map(i => this.start - i), take(this.start + 1), takeUntil(this.pause$))
      .subscribe(i => {
        this.timer.state = TimerState.INPROGRESS;
        const seconds = Number(i);
        const minutes = this.timer.time.min.value - 1;

        if (seconds % 10 === 0) {
          this.notifyProgressEvent();
        }

        if (seconds === 0 ) {
          if (minutes === -1) {
          this.onCountdownComplete();
          } else {
            this.countDown();
          }
        }
        this.timer.time.setSeconds(seconds);

        if (seconds === 59){
          this.timer.time.setMinutes(minutes);
        }

      });

    return {
      resume() {
        $this.start = $this.timer.time.sec.value;
        $this.timer.state = TimerState.INPROGRESS;
      },
      pause() {
        $this.pause$.next(true);
        $this.timer.state = TimerState.PAUSED;
      }
    };
  }


  onCountdownComplete() {
    this.notifyProgressEvent();
    this.playCountdownEndSound();
    this.pause$.next(true);
   this.notifyCountdownEndEvent(this.timer.task);
  }


  updateCountdownTime(time: number) {
    if (!isUndefined(this.timer)) {
      this.timer.countdownTime = time;
      this.timer.time.setMinutes(time);
      this.notifyProgressEvent();
      return this.timer.countdownTime;
    }
    return 25;

  }

  killCountdown() {
    this.timer = null;
    this.pause$.next(true);
    this.notifyProgressEvent(true);
  }

  playCountdownEndSound() {
    const sound = new Howl({
      src: ['../assets/sounds/alarm1.mp3'],
      html5 : true
    });
// Fires when the sound finishes playing.
    sound.on('end', function() {

    });

    sound.play();
  }

  /**
   * trigger event to progress change to update progress bar
   *
   */
  notifyProgressEvent(reset?: boolean) {
    console.log(this.timer)
    if (isBoolean(reset) && reset === true) {
      this.progressEvent.next(0);
      return false;
    }
    const countdownTimeSeconds = this.timer.countdownTime * 60;
    const secondsElapsed = countdownTimeSeconds - ((this.timer.time.min.value * 60) + (this.timer.time.sec.value));
    const percentageComplete = (secondsElapsed  / countdownTimeSeconds) * 100;

    this.progressEvent.next(Math.round(percentageComplete));
  }

  /**s
   * trigger event to begging session
   *
   */
  notifyStartTimerEvent(type: TimerType) {
    this.startTimerEvent.next(type);
  }


  /**
   * trigger event to notify end of session
   *
   */
  notifyCountdownEndEvent(task: Task) {
    this.countdownEndEvent.next(task);
  }

}
