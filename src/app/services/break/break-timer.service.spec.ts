import { TestBed, inject } from '@angular/core/testing';

import { BreakTimerService } from './break-timer.service';

describe('SessionTimerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BreakTimerService]
    });
  });

  it('should be created', inject([BreakTimerService], (service: BreakTimerService) => {
    expect(service).toBeTruthy();
  }));
});
