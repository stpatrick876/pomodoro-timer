import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {SessionTimerService} from '../../services/session/session-timer.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../../services/auth/auth.service';
import {map} from 'rxjs/operators';
import {TaskStatus} from '../../models/common.enum';
import {Task} from 'src/app/models/Task.model';
import ThenableReference = firebase.database.ThenableReference;

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  taskStatus =  TaskStatus;
  taskName: string;
  tasksRef: AngularFireList<any>;
  tasks: Task[];
  user: any;
  @Output() selectTask = new EventEmitter();

  constructor(private timerSrv: SessionTimerService, private afd: AngularFireDatabase, private _authSrv: AuthService) {}

  ngOnInit() {

    this.timerSrv.sessionEndEventAnnounced$.subscribe(task => {
      task.sessionCt = task.sessionCt + 1;
      this.tasksRef.update(task.id, { task: task });
    });

    this._authSrv.user.subscribe(user => {
      this.user = user;
      this.getTasks();
    });
  }
  private getTasks() {
    if (!this.user) {return false;}
    this.tasksRef = this.afd.list('/tasks', ref =>
      ref.orderByChild('task/lastActivity')
    );

    this.tasksRef
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};
        });
      }))
      .subscribe(res => {
        this.tasks = Task.buildTasks(res);
      });
  }
  getIconClass(status: TaskStatus): string{
    let retClass: string;
    switch (status){
      case TaskStatus.COMPLETED:
        retClass = 'icon-complete';
        break;
      case TaskStatus.INPROGRESS:
        retClass = 'icon-active';
        break;
      case TaskStatus.CANCELED:
        retClass = 'icon-canceled';
        break;
    }
    return retClass;
  }

  addTask(name: string) {
    if (!this.user) {return false;}
    const task: Task = new Task();
    task.name = name;
    task.status = TaskStatus.INPROGRESS;
    task.lastActivity = new Date().getTime();
    task.sessionCt = 0;
    task.user = this.user.uid;
    task.isAnonymousUser = this.user.isAnonymous;
    this.taskName = '';
     return this.tasksRef.push({ task: task});
  }

  onSelectTask(task: Task): void {
    task.status = TaskStatus.INPROGRESS;
    this.tasksRef.update(task.id, { task: task }).then(() => {
      this.selectTask.emit(task);
    });
  }

  completeTask(task: Task){
    task.status = TaskStatus.COMPLETED;
    this.tasksRef.update(task.id, { task: task });
  }

  removeTask(task: Task) {
    this.tasksRef.remove(task.id);
  }

  addTaskAndStartSession(taskName: string) {
    (this.addTask(taskName) as any).then((ref) => {
      const task = this.tasks.filter((t) => t.id === ref.key)[0];
      this.onSelectTask(task);
    });
  }
}
