import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {MatDialogRef} from '@angular/material';
import {WelcomeComponent} from '../welcome/welcome.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginErr: boolean;
  constructor(private _authSrv: AuthService, public dialogRef: MatDialogRef<WelcomeComponent>) { }

  ngOnInit() {
  }
  login() {
    this._authSrv.googleLogin().then(res => {
      this.dialogRef.close();
    });
  }

  loginAnon(){
    this._authSrv.anonLogin().then(res => {
      this.dialogRef.close();
    }, err => {
      this.loginErr = true;

      setTimeout(() => {
        this.loginErr = false;
      }, 4000);
    });
  }
}
