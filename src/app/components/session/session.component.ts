import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SessionTimerService} from '../../services/session/session-timer.service';
import {AuthService} from '../../services/auth/auth.service';
import {Timer} from '../../models/Timer';
import {Task} from 'src/app/models/Task.model';
import {TimerState, TimerType} from '../../models/common.enum';
import {BreakTimerService} from '../../services/break/break-timer.service';
import {ClockComponent} from '../clock/clock.component';
import {DEFAULt_BREAK_LENGTH, DEFAULt_SESSION_LENGTH} from '../../models/constants';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  @Input() task: Task;
  sessionLength = DEFAULt_SESSION_LENGTH;
  timer: Timer;
  timerStates = TimerState;
  @ViewChild(ClockComponent) progressClock: ClockComponent;

  constructor(private _sessionTimerSrv: SessionTimerService, private _breakTimerSrv: BreakTimerService,  private _elementRef: ElementRef,
              private render:  Renderer2, private _authSrv: AuthService) {
  }

  ngOnInit() {
    this._breakTimerSrv.startTimerEventAnnounced$.subscribe(() => {
      this.resetCountdown();
    });
    this._sessionTimerSrv.progressEventAnnounced$.subscribe((progress: number) => {
      if (this.progressClock) {
        this.progressClock.renderProgress(progress);
      }
    });
    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state === false) {
        this.resetCountdown();
      }
    });
  }

  onSessionLengthValChange(e) {
    this.sessionLength = e;
    this._sessionTimerSrv.updateCountdownTime(e);
  }

  toggleCountdown() {
    const pauseResumeBtn = this._elementRef.nativeElement.querySelector('button.pause');
    if (!this.timer) {
      this.timer = this._sessionTimerSrv.init(this.task, this.sessionLength, TimerType.SESSION);
      this._sessionTimerSrv.timer$.next(this.timer);
      this._sessionTimerSrv.countDown(TimerType.SESSION);
      this._sessionTimerSrv.notifyStartTimerEvent(TimerType.SESSION);
      pauseResumeBtn.textContent = 'Stop';
    } else if (this.timer && this.timer.state === TimerState.INPROGRESS) {
      this._sessionTimerSrv.countDown(TimerType.SESSION).pause();
      pauseResumeBtn.textContent = 'Start';
    } else if (this.timer && this.timer.state === TimerState.PAUSED) {
      this._sessionTimerSrv.countDown(TimerType.SESSION).resume();
      pauseResumeBtn.textContent = 'Stop';
    }
  }

  resetCountdown() {
    const pauseResumeBtn = this._elementRef.nativeElement.querySelector('button.pause');
    this._sessionTimerSrv.killCountdown();
    this.timer = undefined;
    this.sessionLength = DEFAULt_SESSION_LENGTH;
    pauseResumeBtn.textContent = 'Start';
  }

}
