import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {BreakTimerService} from '../../services/break/break-timer.service';
import {Task} from 'src/app/models/Task.model';
import {Timer} from 'src/app/models/Timer';
import {TimerState, TimerType} from 'src/app/models/common.enum';
import {SessionTimerService} from '../../services/session/session-timer.service';
import {ClockComponent} from '../clock/clock.component';
import {DEFAULt_BREAK_LENGTH} from '../../models/constants';

@Component({
  selector: 'app-break',
  templateUrl: './break.component.html',
  styleUrls: ['./break.component.scss']
})
export class BreakComponent implements OnInit {
  @Input() task: Task;
  breakLength = DEFAULt_BREAK_LENGTH;
  timer: Timer;
  @ViewChild(ClockComponent) progressClock: ClockComponent;

  constructor(private _breakTimerSrv: BreakTimerService, private _sessionTimerSrv: SessionTimerService, private _elementRef: ElementRef,
              private render:  Renderer2, private _authSrv: AuthService) {
  }

  ngOnInit() {
    this._sessionTimerSrv.startTimerEventAnnounced$.subscribe(() => {
      this.resetCountdown();
    });

    this._breakTimerSrv.progressEventAnnounced$.subscribe((progress: number) => {
      if (this.progressClock) {
        this.progressClock.renderProgress(progress);
      }
    });

    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state === false) {
        this._breakTimerSrv.killCountdown();
        this.timer = undefined;
      }
    });
  }

  onBreakLengthValChange(e) {
    this.breakLength = e;
    this._breakTimerSrv.updateCountdownTime(e);
  }

  toggleCountdown() {
    const pauseResumeBtn = this._elementRef.nativeElement.querySelector('button.pause');
    if (!this.timer) {
      this.timer = this._breakTimerSrv.init(this.task, this.breakLength, TimerType.BREAK);
      this._breakTimerSrv.timer$.next(this.timer);
      this._breakTimerSrv.countDown(TimerType.BREAK);
      this._breakTimerSrv.notifyStartTimerEvent(TimerType.SESSION);
      pauseResumeBtn.textContent = 'Stop';
    } else if (this.timer && this.timer.state === TimerState.INPROGRESS) {
      this._breakTimerSrv.countDown(TimerType.BREAK).pause();
      pauseResumeBtn.textContent = 'Start';
    } else if (this.timer && this.timer.state === TimerState.PAUSED) {
      this._breakTimerSrv.countDown(TimerType.BREAK).resume();
      pauseResumeBtn.textContent = 'Stop';
    }
  }

  resetCountdown() {
    const pauseResumeBtn = this._elementRef.nativeElement.querySelector('button.pause');
    this._breakTimerSrv.killCountdown();
    this.timer = undefined;
    this.breakLength = DEFAULt_BREAK_LENGTH;
    pauseResumeBtn.textContent = 'Start';
  }

}
