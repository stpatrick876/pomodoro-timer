import {Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {SessionTimerService} from '../../services/session/session-timer.service';
import {Timer} from '../../models/Timer';
import {TimerState} from '../../models/common.enum';


@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {
  @Input() timer: Timer;
  @Input() countdownTime: number;
  states = TimerState;

  constructor(private _timerSrv: SessionTimerService, private _elementRef: ElementRef, private render:  Renderer2) { }

  ngOnInit() {}

   renderProgress(progress: number) {
     console.log('progress 2 ', progress)

     const el = this._elementRef.nativeElement.querySelector('#PomodoroClock');
    this.render.setAttribute(el, 'data-progress', progress.toString());
  }
}
