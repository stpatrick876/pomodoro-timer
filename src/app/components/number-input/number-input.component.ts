import {Component, ElementRef, OnInit, Renderer2, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.scss']
})
export class NumberInputComponent implements OnInit {
  counter: number;
  els: any = {};
  @Output() valChange: EventEmitter<number>  = new EventEmitter<number>();
  @Input() value: number;

  constructor(private el: ElementRef, private renderer2: Renderer2) {
  }

  ngOnInit() {

    this.counter = this.value ? this.value : 0;
    this.els = {
      decrement: this.el.nativeElement.querySelector('.ctrl__button--decrement'),
      counter: {
        container: this.el.nativeElement.querySelector('.ctrl__counter'),
        num: this.el.nativeElement.querySelector('.ctrl__counter-num'),
        input: this.el.nativeElement.querySelector('.ctrl__counter-input')
      },
      increment: this.el.nativeElement.querySelector('.ctrl__button--increment')
    };
    this.init();
  }

  decrement() {
    let counter = this.getCounter();
    let nextCounter = (this.counter > 0) ? --counter : counter;
    this.setCounter(nextCounter);
  };

  increment() {
    let counter = this.getCounter();
    const nextCounter = (counter < 9999999999) ? ++counter : counter;
    this.setCounter(nextCounter);
  };

  getCounter() {
    return this.counter;
  };

  setCounter(nextCounter) {
    this.counter = nextCounter;
    this.valChange.emit(this.counter);
  };

  debounce(callback) {
    setTimeout(callback, 100);
  };

  /*
  * Toggle classes to show/animate display value
  */
  render(hideClassName?: string, visibleClassName?: string) {
    this.renderer2.addClass(this.els.counter.num, hideClassName)

    setTimeout(() => {
      this.els.counter.num.innerText = this.getCounter();
      this.els.counter.input.value = this.getCounter();
      this.renderer2.addClass(this.els.counter.num, visibleClassName);
    }, 100);

    setTimeout(() => {
      this.renderer2.removeClass(this.els.counter.num, hideClassName);
      this.renderer2.removeClass(this.els.counter.num, visibleClassName);
    }, 1100);
  };
  /*
  *Initialize listeners to trgger value change
  **/
  init() {
    this.renderer2.listen(this.els.decrement, 'click',() => {
      this.debounce(() => {
        this.decrement();
        this.render('is-decrement-hide', 'is-decrement-visible');
      });
    });

    this.renderer2.listen(this.els.increment, 'click',() => {
      this.debounce(() => {
        this.increment();
        this.render('is-increment-hide', 'is-increment-visible');
      });
    });

    this.renderer2.listen(this.els.counter.input, 'input',(e) => {
      let parseValue = parseInt(e.target.value);
      if (!isNaN(parseValue) && parseValue >= 0) {
        this.setCounter(parseValue);
        this.render();
      }
    });


    this.renderer2.listen(this.els.counter.input, 'focus',(e) => {
      this.renderer2.addClass(this.els.counter.container, 'is-input');
    });

    this.renderer2.listen(this.els.counter.input, 'blur',(e) => {
      this.renderer2.removeClass(this.els.counter.container, 'is-input');
      this.render();

    });

  };

}
