import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatTabGroup} from '@angular/material';
import {AuthService} from './services/auth/auth.service';
import {SessionTimerService} from './services/session/session-timer.service';
import {LoginComponent} from './components/login/login.component';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {Task} from './models/Task.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  selectedTask: Task;
  user: any;
  welcomeDialogRef: MatDialogRef<WelcomeComponent>;
  @ViewChild(MatTabGroup) tabs: MatTabGroup;

  constructor(private timerSrv: SessionTimerService, private authDialog: MatDialog,
              private _authSrv: AuthService, private elementRef: ElementRef, private render:  Renderer2) {
    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state === false) {
        localStorage.removeItem('pomodoro-intro');
      }
    });
  }

  ngOnInit() {
    this._authSrv.user.subscribe(user => {
      this.user = user;
    });

    if (localStorage.getItem('pomodoro-intro') !== 'true'){
      this.welcomeDialogRef = this.authDialog.open(WelcomeComponent, {
        width: '80%',
        height: '80%',
        backdropClass: 'dialog-backdrop',
        disableClose: true
      });

      this.welcomeDialogRef.afterClosed().subscribe(() => {
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
      });

      this.welcomeDialogRef.afterOpen().subscribe(() => {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        localStorage.setItem('pomodoro-intro', 'true');
      });
    }
  }

  logout() {
    this._authSrv.logout().then(res => {
      this._authSrv.notifyLogInOutEvent(false);
    });
  }

  /**
   * handle login via firebase
   */
  login() {
    this.render.removeClass(this.elementRef.nativeElement.querySelector('#loginBtn'), 'highlight-btn');

    const dialogRef = this.authDialog.open(LoginComponent, {
      width: '80%',
      height: '80%'
    });
    dialogRef.afterClosed().subscribe(() => {
      document.getElementsByTagName('body')[0].classList.remove('modal-open');
    });

    dialogRef.afterOpen().subscribe(() => {
      document.getElementsByTagName('body')[0].classList.add('modal-open');
    });
  }

  handleTaskChange(task: Task) {
    this.selectedTask = task;
    this.tabs.selectedIndex = 1;
  }
}
