import {TimerState, TimerType} from './common.enum';
import {Time} from './Time.model';
import {Task} from './Task.model';

export class Timer {
  time: Time;
  countdownTime: number;
  state: TimerState;
  task: Task;
  type: TimerType;

  constructor() {}

  initializeTimer(startTime: number, task: Task, type: TimerType) {
    this.countdownTime = startTime;
    this.task = task;
    this.type = type;
    this.time = {
      min: {
        value: 0,
        displayValue: '00'
      },
      sec: {
        value: 0,
        displayValue: '00'
      },
      setSeconds(seconds: number) {
        this.sec.value = seconds;

        if (seconds < 10) {
          this.sec.displayValue = `0${seconds}`;
        } else {
          this.sec.displayValue = `${seconds}`;

        }
      },
      setMinutes(minutes: number) {
        this.min.value = minutes;

        if (minutes < 10) {
          this.min.displayValue = `0${minutes}`;
        } else {
          this.min.displayValue = `${minutes}`;

        }
      }
    };

    this.time.setMinutes(startTime);
    this.state = TimerState.NOTSTARTED;
    return this;
  }
}
