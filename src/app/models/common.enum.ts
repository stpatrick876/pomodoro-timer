export enum TimerState {
  COMPLETE,
  PAUSED,
  INPROGRESS,
  NOTSTARTED,
  ONBREAK
}

export enum TaskStatus {
  COMPLETED,
  INPROGRESS,
  CANCELED
}

export enum TimerType {
  SESSION,
  BREAK
}
