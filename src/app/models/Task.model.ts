import {TaskStatus} from './common.enum';

export class Task {
  id: string;
  name: string;
  status: TaskStatus;
  sessionCt: number;
  lastActivity: number;
  user: string;
  isAnonymousUser: boolean;

  static buildTasks(dbList: any): Task[] {
     return dbList.map((obj) => {
      const task = new Task();
      task.id = obj.key;
      task.name = obj.task.name;
      task.status = obj.task.status;
      task.sessionCt = obj.task.sessionCt;
      task.lastActivity = obj.task.lastActivity;
      task.user = obj.task.user;
      if(typeof obj.task.isAnonymousUser !== 'undefined'){
        task.isAnonymousUser = obj.task.isAnonymousUser;
      }
      return task;
    });
  }
}
