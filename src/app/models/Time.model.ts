export interface Time {
  min: {
    value: number;
    displayValue: string;
  };
  sec: {
    value: number;
    displayValue: string;
  };
  setSeconds(seconds: number): void;
  setMinutes(minutes: number): void;
};
